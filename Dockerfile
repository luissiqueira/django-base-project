FROM roove/ubuntu-py3.8:latest

RUN apt-get update --fix-missing
RUN apt-get install -y sqlite3

ADD requirements.txt /src/project/requirements.txt

RUN cd /src/project; pip install pip --upgrade
RUN cd /src/project; pip3 install -r requirements.txt --upgrade

RUN apt-get update --fix-missing
RUN apt-get install -y libgl1-mesa-dev

ADD . /src/project

RUN chmod -R +x /src/project/deploy/services/
RUN chmod -R +x /src/project/deploy/init/
RUN cp -R /src/project/deploy/services/* /etc/service/
RUN cp -R /src/project/deploy/init/* /etc/my_init.d/

# For dokku applications
ADD CHECKS /app/CHECKS

RUN mkdir /var/www/static/
RUN mkdir /var/www/spooler/
RUN chown -R www-data /var/www

RUN cp /src/project/deploy/nginx.conf /etc/nginx/

RUN rm /etc/nginx/sites-enabled/default

RUN ln -s /src/project/deploy/django.conf /etc/nginx/sites-enabled/django.conf

EXPOSE 80

CMD ["/sbin/my_init"]
