# django-base-project

A minimal django boilerplate with lint, coverage and Dockerfile for simple applications.

## How to use

```
# Get the code
git clone https://github.com/luissiqueira/django-base-project.git
cd django-base-project

# Virtualenv modules installation (Unix based systems)
python3 -m venv venv
source venv/bin/activate

# Virtualenv modules installation (Windows based systems)
virtualenv --no-site-packages env
.\venv\Scripts\activate

# Install modules
pip install -r requirements.txt

# Configure .env file
cp project/.env.sample project/.env

# Create tables
python manage.py makemigrations
python manage.py migrate

python manage.py createsuperuser

# Start the application (development mode)
python manage.py runserver # default port 8000

# Start the app - custom port 
# python manage.py runserver 0.0.0.0:<your_port>

# Access the web app in browser: http://127.0.0.1:8000/
```

## Ubuntu dependencies

```
# for postgres as database
sudo apt get install -y postgres postgres-contrib
sudo apt get install -y postgresql-server-dev-10
sudo apt get install -y python3-dev

# for lint
sudo apt get install -y flake8
```

## Scripts

```
# for run lint
flake8

# for run coverage with html export
python -m coverage html
```